package back.offfice;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.ClassResource;
import com.vaadin.ui.*;

public class CommonPage extends VerticalLayout implements View {

    private static final String URL = "http://163.172.156.224:8080/";
    private static final String imageWidth = "70%";

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        Object userName = UI.getCurrent().getSession().getAttribute("userName");
        if(userName == null|| userName.toString().isEmpty()){
            getUI().getNavigator().navigateTo("login");
        }
    }

    public CommonPage(){
        Object userName = UI.getCurrent().getSession().getAttribute("userName");
        String redirectURL = "/login?userName="+userName.toString();
        VerticalLayout verticalLayout = new VerticalLayout();
        HorizontalLayout btnLayout = new HorizontalLayout();
        Image logo = new Image();
        logo.setSource(new ClassResource("w-logo.svg"));

        CssLayout IconWrapper = new CssLayout();
        Label skyText = new Label("INFLIGHT");
        Image skybutton = new Image(null, new ClassResource("flight.svg"));
        IconWrapper.addStyleName("IconWrapper");
        skyText.addStyleName("icon-text");
        skybutton.addStyleName("my-img-button");
        IconWrapper.addComponents(skybutton, skyText);

        skybutton.addClickListener(clickEvent -> {
            getUI().getPage().setLocation(URL+"back-office"+redirectURL);
        });

        Image cloud = new Image(null, new ClassResource("cloud.svg"));
        Image cloud1 = new Image(null, new ClassResource("cloud.svg"));
        Image cloud2 = new Image(null, new ClassResource("cloud.svg"));
        cloud.setStyleName("cloud");
        cloud1.setStyleName("cloud1");
        cloud2.setStyleName("cloud2");

        CssLayout IconWrapper2 = new CssLayout();
        Label skyText2 = new Label("AIRPORT");
        Image groundApp = new Image(null, new ClassResource("flight_b.svg"));
        IconWrapper2.addStyleName("IconWrapper boarder-icon");
        skyText2.addStyleName("icon-text");
        groundApp.addStyleName("my-img-button");
        IconWrapper2.addComponents(groundApp, skyText2);

        groundApp.addClickListener(clickEvent -> {
            getUI().getPage().setLocation(URL+"aerotech-ground"+redirectURL);
        });
        CssLayout IconWrapper3 = new CssLayout();
        Label skyText3 = new Label("FLIGHT KITCHEN");
        Image flightK_button = new Image(null, new ClassResource("flight_cn.svg"));
        IconWrapper3.addStyleName("IconWrapper");
        skyText3.addStyleName("icon-text");
        flightK_button.addStyleName("flight_cn");
        IconWrapper3.addComponents(flightK_button, skyText3);


        flightK_button.addClickListener(clickEvent -> {
            Notification.show("Not implemented yet.", Notification.Type.WARNING_MESSAGE);
        });

        btnLayout.setSizeFull();
        verticalLayout.setStyleName("wrappwr-button");
        verticalLayout.addComponent(btnLayout);
        btnLayout.addComponents(IconWrapper,IconWrapper2,IconWrapper3);
        btnLayout.setComponentAlignment(IconWrapper,Alignment.MIDDLE_CENTER);
        btnLayout.setComponentAlignment(IconWrapper2,Alignment.MIDDLE_CENTER);
        btnLayout.setComponentAlignment(IconWrapper3,Alignment.MIDDLE_CENTER);
        logo.setHeight(195, Unit.PIXELS);
        logo.setStyleName("common-logo");

        addComponent(logo);
        addComponents(cloud, cloud1, cloud2, verticalLayout);
        setStyleName("bg_banner");
        setSizeFull();
        setComponentAlignment(verticalLayout, Alignment.MIDDLE_CENTER);
        setComponentAlignment(logo,Alignment.TOP_CENTER);
    }
}
