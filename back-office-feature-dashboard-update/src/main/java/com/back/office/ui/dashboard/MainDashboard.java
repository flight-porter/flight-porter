package com.back.office.ui.dashboard;

import com.back.office.entity.SubMenuItem;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;

import java.util.LinkedHashMap;
import java.util.Map;

public class MainDashboard extends VerticalLayout implements View {

    private static final String imageWidth = "35%";

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        Object userName = UI.getCurrent().getSession().getAttribute("userName");
        if(userName == null|| userName.toString().isEmpty()){
            getUI().getNavigator().navigateTo("login");
        }
    }

    public MainDashboard(){
        createMainLayout();
    }

    private void createMainLayout(){
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        HorizontalLayout btnLayout1 = new HorizontalLayout();
        btnLayout1.setSizeFull();
        HorizontalLayout btnLayout2 = new HorizontalLayout();
        btnLayout2.setSizeFull();
        btnLayout2.setHeight("260px");
        verticalLayout.setStyleName("main-layout");
//        btnLayout1.setMargin(Constants.bottomMarginInfo);
        verticalLayout.addComponent(btnLayout1);
        verticalLayout.addComponent(btnLayout2);



        CssLayout iconWrapper1 = new CssLayout();
        Button flightKitchenImage = new Button("flight kitchen");
        iconWrapper1.addComponents(flightKitchenImage);
        iconWrapper1.setStyleName("iconWrapper-1");
        flightKitchenImage.setIcon(FontAwesome.RANDOM);
        flightKitchenImage.addClickListener(clickEvent -> {
            CssLayout iconWrapper4 = new CssLayout();
            SubMenuItem menuItem = new SubMenuItem();
            menuItem.setMenuImage("crm_sub.svg");
            Map<String,String> row1Map = new LinkedHashMap<>();
            row1Map.put("Passenger Purchases","PassengerPurchases");
            row1Map.put("Import pax Manifest","");
            row1Map.put("Loading Recommendations","");
            row1Map.put("SIF","SIFDetails");
            row1Map.put("HHC and Cart Usage","HHCAndCartUsage");
            menuItem.setMenuName("flight_kitchen");
            menuItem.setSubMenuImageMap(row1Map);
            UI.getCurrent().getSession().setAttribute("subMenu",menuItem);
            getUI().getNavigator().navigateTo("CommonView");
        });


        CssLayout iconWrapper2 = new CssLayout();
        Button preOrderImage = new Button("pre order");
        iconWrapper2.addComponents(preOrderImage );
        iconWrapper2.setStyleName("iconWrapper-2");
        preOrderImage.setIcon(FontAwesome.CLOCK_O);

        preOrderImage.addClickListener(clickEvent -> {

            SubMenuItem menuItem = new SubMenuItem();
            menuItem.setMenuImage("pre_order_sub.svg");
            Map<String,String> row1Map = new LinkedHashMap<>();
            row1Map.put("Pre-order","PreOrders");
            row1Map.put("Pre-order Inventory","");
            row1Map.put("Inflight Requests","MessagingModule");
            row1Map.put("Bond Messages","BondMessages");
            row1Map.put("FA Messages","FAMessages");
            menuItem.setSubMenuImageMap(row1Map);
            menuItem.setMenuName("pre_order");
            UI.getCurrent().getSession().setAttribute("subMenu",menuItem);
            getUI().getNavigator().navigateTo("CommonView");
        });



        CssLayout iconWrapper3 = new CssLayout();
        Button financeImage = new Button("finance");
        iconWrapper3.addComponents(financeImage);
        iconWrapper3.setStyleName("iconWrapper-3");
        financeImage.setIcon(FontAwesome.BAR_CHART);

        financeImage.addClickListener(clickEvent -> {

            SubMenuItem menuItem = new SubMenuItem();
            menuItem.setMenuImage("finance_sub.svg");
            Map<String,String> row1Map = new LinkedHashMap<>();
            row1Map.put("Currency History","CurrencyHistory");
            row1Map.put("Bank Settlements","");
            row1Map.put("CC Batch Summary","");
            row1Map.put("FA Commissions","");
            row1Map.put("Gross Margins","GrossMargins");
            row1Map.put("Sales Tender Discrepancy","");
            menuItem.setSubMenuImageMap(row1Map);
            menuItem.setMenuName("finance");
            UI.getCurrent().getSession().setAttribute("subMenu",menuItem);
            getUI().getNavigator().navigateTo("CommonView");
        });


        CssLayout iconWrapper4 = new CssLayout();
        Button bondReportsImage = new Button("reports");
        iconWrapper4.addComponents(bondReportsImage);
        iconWrapper4.setStyleName("iconWrapper-4");
        bondReportsImage.setIcon(FontAwesome.FILE_TEXT_O);


        bondReportsImage.addClickListener(clickEvent -> {

            SubMenuItem menuItem = new SubMenuItem();
            menuItem.setMenuImage("reports_sub.svg");
            Map<String,String> row1Map = new LinkedHashMap<>();
            row1Map.put("Monthly Sales","MonthlySales");
            row1Map.put("Flight Sales","FlightSales");
            row1Map.put("Category/Sector Sales","");
            row1Map.put("Item Sales","ItemSales");
            row1Map.put("FA Performance","");
            row1Map.put("Tender Summary","");
            menuItem.setSubMenuImageMap(row1Map);
            menuItem.setMenuName("report");
            UI.getCurrent().getSession().setAttribute("subMenu",menuItem);
            getUI().getNavigator().navigateTo("CommonView");

        });


        CssLayout iconWrapper5 = new CssLayout();
        Button inventoryImage = new Button("inventory");
        iconWrapper5.addComponents(inventoryImage);
        iconWrapper5.setStyleName("iconWrapper-4");
        inventoryImage.setIcon(FontAwesome.BUILDING);

        inventoryImage.addClickListener(clickEvent -> {

            SubMenuItem menuItem = new SubMenuItem();
            menuItem.setMenuImage("inventory_sub.svg");
            Map<String,String> row1Map = new LinkedHashMap<>();
            row1Map.put("Equipment Master","EquipmentMaster");
            row1Map.put("Month End  Inventory","");
            row1Map.put("Unaccounted Carts","");
            row1Map.put("Inventory Valuation","");
            row1Map.put("Sold-out by Flight","SoldOut");
            row1Map.put("Custom Reports","");
            menuItem.setMenuName("inventory");
            menuItem.setSubMenuImageMap(row1Map);
            UI.getCurrent().getSession().setAttribute("subMenu",menuItem);
            getUI().getNavigator().navigateTo("CommonView");

        });



        CssLayout iconWrapper6 = new CssLayout();
        Button crmImage = new Button("crm");
        iconWrapper6.addComponents(crmImage);
        iconWrapper6.setStyleName("iconWrapper-3");
        crmImage.setIcon(FontAwesome.UMBRELLA);

        crmImage.addClickListener(clickEvent -> {
            SubMenuItem menuItem = new SubMenuItem();
            menuItem.setMenuImage("crm_random-solid.svg");
            Map<String,String> row1Map = new LinkedHashMap<>();
            row1Map.put("Passenger Purchases","PassengerPurchases");
            row1Map.put("Import pax Manifest","");
            row1Map.put("Loading Recommendations","");
            menuItem.setMenuName("crm");
            menuItem.setSubMenuImageMap(row1Map);
            UI.getCurrent().getSession().setAttribute("subMenu",menuItem);
            getUI().getNavigator().navigateTo("CommonView");
        });


        CssLayout iconWrapper7 = new CssLayout();
        Button analysisImage = new Button("analyze upload");
        iconWrapper7.addComponents(analysisImage);
        iconWrapper7.setStyleName("iconWrapper-2");
        analysisImage.setIcon(FontAwesome.ARCHIVE);

        analysisImage.addClickListener(clickEvent -> {
            SubMenuItem menuItem = new SubMenuItem();
            menuItem.setMenuImage("analyze_upload_sub.svg");
            Map<String,String> row1Map = new LinkedHashMap<>();
            row1Map.put("Wastage","Wastage");
            row1Map.put("SIF Inquiry","");
            row1Map.put("Build Times","BuildTimes");
            row1Map.put("Sales vs Weight","");
            row1Map.put("Airline Uploads","");
            row1Map.put("Custom Reports","");
            menuItem.setMenuName("analyze");
            menuItem.setSubMenuImageMap(row1Map);
            UI.getCurrent().getSession().setAttribute("subMenu",menuItem);
            getUI().getNavigator().navigateTo("CommonView");
        });
        CssLayout iconWrapper8 = new CssLayout();
        Button settingsImage = new Button("setup");
        iconWrapper8.addComponents(settingsImage);

        iconWrapper8.setStyleName("iconWrapper-1");
        settingsImage.setIcon(FontAwesome.COG);


        settingsImage.addClickListener(clickEvent -> {
            SubMenuItem menuItem = new SubMenuItem();
            menuItem.setMenuImage("setup_sub.svg");
            Map<String,String> row1Map = new LinkedHashMap<>();
            row1Map.put("Aircraft Type","AircraftType");
            row1Map.put("Flight Details","FlightDetails");
            row1Map.put("Currency","Currency");
            row1Map.put("Create Items","CreateItems");
            row1Map.put("Equipment Type","EquipmentTypes");
            row1Map.put("Assign Items","AssignItems");
            row1Map.put("Create KIT Codes","CreateKitCodes");
            row1Map.put("CC Blacklist","CCBlackList");
            row1Map.put("Promotions","Promotions");
            row1Map.put("Vouchers","Vouchers");
            row1Map.put("FA Commission Table","error");
            row1Map.put("Budget","Budget");
            menuItem.setMenuName("setup");
            menuItem.setSubMenuImageMap(row1Map);
            UI.getCurrent().getSession().setAttribute("subMenu",menuItem);
            getUI().getNavigator().navigateTo("CommonView");
        });


        btnLayout1.addComponents(iconWrapper1,iconWrapper2,iconWrapper3,iconWrapper4);
        btnLayout1.setComponentAlignment(iconWrapper1, Alignment.MIDDLE_CENTER);
        btnLayout1.setComponentAlignment(iconWrapper2,Alignment.MIDDLE_CENTER);
        btnLayout1.setComponentAlignment(iconWrapper3,Alignment.MIDDLE_CENTER);
        btnLayout1.setComponentAlignment(iconWrapper4,Alignment.MIDDLE_CENTER);

        btnLayout2.addComponents(iconWrapper5,iconWrapper6,iconWrapper7,iconWrapper8);
        btnLayout2.setComponentAlignment(iconWrapper5, Alignment.MIDDLE_CENTER);
        btnLayout2.setComponentAlignment(iconWrapper6,Alignment.MIDDLE_CENTER);
        btnLayout2.setComponentAlignment(iconWrapper7,Alignment.MIDDLE_CENTER);
        btnLayout2.setComponentAlignment(iconWrapper8,Alignment.MIDDLE_CENTER);

//        MarginInfo info = new MarginInfo(true);
//        info.setMargins(true,false,false,false);
//        verticalLayout.setMargin(info);
        addComponent(verticalLayout);
        setComponentAlignment(verticalLayout, Alignment.MIDDLE_CENTER);
    }
}
